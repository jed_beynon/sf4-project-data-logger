import csv
import tkinter.filedialog as tkf
import time

data = [0, 1, 2]


print(time.strftime("%Y%m%d-%H%M%S"))

fileTypes = [
                ("CSV", "*.csv"),
                ("Text Document", "*.txt")
                
            ]

filename = tkf.asksaveasfilename(initialfile=time.strftime("%Y%m%d-%H%M%S"), defaultextension=fileTypes, filetypes=fileTypes)

print(filename)