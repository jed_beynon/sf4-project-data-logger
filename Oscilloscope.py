import _thread
import csv
import time
import tkinter as tk
import tkinter.filedialog as tkf
from dataclasses import dataclass
import matplotlib.pyplot as plt
import numpy as np
import serial
import serial.tools.list_ports
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from numpy.fft import fft
from scipy.signal import find_peaks


# Global settings
@dataclass
class settingsclass:
    port: str = "COM1"
    deviceName: str = ""
    refreshRate: int = 1000  # millisec
    running: bool = True
    mode: str = "time"
    timeRangeShown: int = 200  # in millisec
    frequencyRangeShown: int = 2000  # in Hz
    xdivisions: float = 100
    trigger: bool = False
    triggerChannel = "A"
    triggerValue: float = 2.5


settings = settingsclass()  # Holds the main GUI settings


@dataclass
class channelSettings:
    visible: bool = True
    yLimitVoltage: float = 5
    yLimitSpectrum: float = 20000


# Holds settings related to the channels
ASettings = channelSettings()
BSettings = channelSettings()


@dataclass
class cursorClass:
    H1Visible: bool = False
    H1Position: float = 2.5
    H2Visible: bool = False
    H2Position: float = 2.5
    V1Visible: bool = False
    V1Position: float = 0
    V2Visible: bool = False
    V2Position: float = 0


cursorSettings = cursorClass()


@dataclass
class measurementClass:
    M1Channel: str = "A"
    M2Channel: str = "A"
    M3Channel: str = "A"
    M4Channel: str = "A"
    M1Type: str = "Off"
    M2Type: str = "Off"
    M3Type: str = "Off"
    M4Type: str = "Off"
    M1Limited: bool = False  # true: calcs done on the data between cursors, false: calcs done on all recorded data
    M2Limited: bool = False
    M3Limited: bool = False
    M4Limited: bool = False


measurementSettings = measurementClass()


# Global variables related to data
# the x axis of the timeplot
t = np.array(range(1024))
freqA = np.array(range(1024))
freqB = np.array(range(1024))
# Incoming data
channelAraw = np.zeros(1024)
channelBraw = np.zeros(1024)
newestID = 0  # The latest datapoint is stored at this index
samplingRate = 3500  # this gets updated when data is received

# Manipulated data
channelA = np.zeros(1024)
channelB = np.zeros(1024)
spectrumA = np.zeros(1024)
spectrumB = np.zeros(1024)


# Options for the dropdown menus
screenRefreshRateOptions = [0.1, 0.2, 0.5, 1, 2, 5]
yLimitVoltageOptions = [0.1, 0.2, 0.5, 1, 2, 5, 6]
yLimitSpectrumOptions = [10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 100000, 200000, 500000, 1000000]
timeRangeOptions = [0.5, 1, 2, 5, 10, 20, 50, 100, 200]  # in millisec
frequencyRangeOptions = [5, 10, 20, 50, 100, 200, 500, 1000, 2000]  # in Hz
measurementOptions = ["Off", "RMS", "Mean", "Peak", "Peak-to-peak", "Spectrum peak", "SNR", "Frequency"]  # , "Rise Time", "Fall Time"]

# Stores the request to refresh the screen (used to cancel a request)
updateJob = None

# Finds an arduino on startup and creates the options for the port selection list
ports = serial.tools.list_ports.comports()
portDict = dict()
for port in ports:
    portDict[port.description] = port.device

    if "Arduino" in port.description:
        settings.port = port.device
        settings.deviceName = port.description

# Initialises serial connection
arduino = serial.Serial()
arduino.baudrate = 1000000
arduino.port = settings.port


def decode(bytes):
    # Decodes a pair of encoded bytes to an integer

    upperByte = bytes[0]*16
    lowerByte = bytes[1]//4
    # print("bytes:", bytes, "upper: ", upperByte, "lower: ", lowerByte)
    return upperByte + lowerByte


def readSerialInput():

    global channeAraw, channelBraw, newestID, samplingRate

    arduino.open()

    while True:

        incoming = arduino.readline()
        if len(incoming) == 7 or len(incoming) == 9:  # Check to see if full line was read

            # Decode incoming bytes and convert to voltage
            dataID = decode(incoming[0:2])
            firstNumber = decode(incoming[2:4])/1023*5.0
            secondNumber = decode(incoming[4:6])/1023*5.0

            # Store decoded integers
            channelAraw[dataID] = firstNumber
            channelBraw[dataID] = secondNumber
            newestID = dataID

        else:
            print("Invalid data received")

        if len(incoming) == 9:  # Longer line with timing data

            samplingTime = decode(incoming[6:8])
            if samplingTime == 0:
                print("Invalid data received")
            else:
                samplingRate = 1e6/samplingTime

        # If port is changed, close and reopen the connection
        if arduino.port != settings.port:
            arduino.close()
            arduino.port = settings.port
            arduino.open()


def shiftData(raw, newestID):
    # Shifts the data so the oldest datapoint is at the 0th index and the newest is at 1023rd
    return np.roll(raw, 1023-newestID)


def spectrum(channel):  # code taken from https://pythonnumericalmethods.berkeley.edu/notebooks/chapter24.04-FFT-in-Python.html
    sr = samplingRate  # sampling rate
    X = fft(channel)  # fft of channel
    N = len(X)
    n = np.arange(N)
    T = N/sr
    freq = n/T  # array of frequencies that will be shown
    return X, freq  # To plot, make freq the x-axis and np.abs(X) the y-axis, and limit the axes as necessary. Use plt.stem()


def getMeasurement(channel, type, limited):
    # Returns one measurement
    if type == "Off":
        return "Off"

    if channel == "A":
        data = channelA
        spectrum = spectrumA
    else:
        data = channelB
        spectrum = spectrumB

    if limited:  # Only measure between the cursors
        lowerCursor = min(cursorSettings.V1Position, cursorSettings.V2Position)
        upperCursor = max(cursorSettings.V1Position, cursorSettings.V2Position)
        if lowerCursor == upperCursor:
            return "Cursors overlapping"
        else:
            # find the indeces closest to the cursors
            for i in range(len(t)):
                if t[i] >= lowerCursor:
                    lowerIndex = i
                    break
            for i in range(len(t)):
                upperIndex = i
                if t[i] >= upperCursor:
                    break

            # only consider the data between the cursors
            data = data[lowerIndex:upperIndex]

    # //////// General Calculations ////////

    # RMS
    if type == "RMS":
        return "{:.2f}".format(np.sqrt(np.mean(data**2))) + " V"

    # Mean
    if type == "Mean":
        return "{:.2f}".format(np.mean(data)) + " V"

    # Peak
    if type == "Peak":
        return "{:.2f}".format(data.max()) + " V"

    # Peak-to-Peak
    if type == "Peak-to-peak":
        return "{:.2f}".format(data.max() - data.min()) + " V"

    # Spectrum Peak
    if type == "Spectrum peak":
        return "{:.2f}".format(np.abs(spectrum.max())) + " V"

    # SNR
    def signaltonoise(Arr, axis=0, ddof=0):  # Code taken from https://stackoverflow.com/questions/63177236/how-to-calculate-signal-to-noise-ratio-using-python
        Arr = np.asanyarray(Arr)
        me = Arr.mean(axis)
        sd = Arr.std(axis=axis, ddof=ddof)
        return np.where(sd == 0, 0, me/sd)

    if type == "SNR":
        return "{:.2f}".format(signaltonoise(data))

    # Frequency
    if type == "Frequency":  # need to double check with sampling rate
        if round(data.max(), 7) == 0.0000000:
            return "DC"
        else:
            peaks, _ = find_peaks(data, height=0.5*data.max())
            if len(peaks) < 2:
                return "DC"
            else:
                adjustedpeaks = peaks/samplingRate
                period = (adjustedpeaks.max()-adjustedpeaks.min())/(len(adjustedpeaks)-1)
                freq = 1/period
                return "{:.2f}".format(freq) + " Hz"

    # Rise Time
    if type == "Rise Time":  # https://stackoverflow.com/questions/50365310/python-rising-falling-edge-oscilloscope-like-trigger
        if round(data.max(), 7) == 0.0000000:
            return "DC"
        else:
            peaks, _ = find_peaks(data, height=0.5*data.max())
            if len(peaks) < 2:
                return "DC"
            else:
                val1 = data.min() + 0.1*(data.max() - data.min())  # 10% of max
                val2 = data.min() + 0.9*(data.max() - data.min())  # 90% of max

                a = np.flatnonzero((data[:-1] < val1) & (data[1:] > val1))+1
                # b = a/samplingRate  # Times of 10% levels

                c = np.flatnonzero((data[:-1] < val2) & (data[1:] > val2))+1
                # d = c/samplingRate  # Times of 90% levels

                rise_time = np.average(data[c]-data[a])
                return rise_time
    # Fall time
    if type == "Fall Time":
        if round(data.max(), 7) == 0.0000000:
            return "DC"
        else:
            peaks, _ = find_peaks(data, height=0.5*data.max())
            if len(peaks) < 2:
                return "DC"
            else:
                val1 = 0.1*data.max()  # 10% of max
                val2 = 0.9*data.max()  # 90% of max

                e = np.flatnonzero((data[:-1] > val1) & (data[1:] < val1))+1
                f = e/samplingRate  # Times of 10% levels

                g = np.flatnonzero((data[:-1] > val2) & (data[1:] < val2))+1
                h = g/samplingRate  # Times of 90% levels

                fall_time = np.average(f-h)
                return fall_time

    # //////// End of General Calculations ////////

    return "Invalid measurement type"  # Should never see this


def updateMeasurements():
    # Updates all four measuremnets

    measurement1Label.config(text=getMeasurement(measurementSettings.M1Channel, measurementSettings.M1Type, measurementSettings.M1Limited))
    measurement2Label.config(text=getMeasurement(measurementSettings.M2Channel, measurementSettings.M2Type, measurementSettings.M2Limited))
    measurement3Label.config(text=getMeasurement(measurementSettings.M3Channel, measurementSettings.M3Type, measurementSettings.M3Limited))
    measurement4Label.config(text=getMeasurement(measurementSettings.M4Channel, measurementSettings.M4Type, measurementSettings.M4Limited))


def updateCanvas():
    global channelA, channelB, updateJob, spectrumA, spectrumB, t, freqA, freqB
    print("Updatecanvas", channelAraw[0], len(channelA), settings.mode)

    # The x axis in time mode
    t = np.arange(start=-settings.timeRangeShown/2, stop=settings.timeRangeShown/2+1000/samplingRate, step=1000.0/samplingRate)

    # Only change the data if running
    if settings.running:
        channelA = shiftData(channelAraw, newestID)
        channelB = shiftData(channelBraw, newestID)
        spectrumA, freqA = spectrum(channelA)
        spectrumB, freqB = spectrum(channelB)

    # Set up the plotting area
    fig.clear()
    aPlot = fig.add_subplot(111)
    aPlot.set_ylabel("Channel A")
    bPlot = aPlot.twinx()
    bPlot.set_ylabel("Channel B")
    aPlot.grid()

    if settings.mode == "time":
        aPlot.set_title("Time")
        aPlot.set_xlim(t[0], t[-1])
        aPlot.set_xlabel("ms")

        aPlot.set_ylim(bottom=-ASettings.yLimitVoltage/20, top=ASettings.yLimitVoltage)
        bPlot.set_ylim(bottom=-ASettings.yLimitVoltage/20, top=BSettings.yLimitVoltage)

        # Trigger
        if settings.trigger:
            # aPlot.plot(t[int(len(t)/2)], settings.triggerValue, marker="*", markersize = 10, color="orange")

            if settings.triggerChannel == "A":
                crossing_points = np.flatnonzero((channelA[:-1] < settings.triggerValue) & (channelA[1:] > settings.triggerValue))  # Find the indexes in Channel A that cross the trigger value on a rising edge
            else:
                crossing_points = np.flatnonzero((channelB[:-1] < settings.triggerValue) & (channelB[1:] > settings.triggerValue))  # Find the indexes in Channel B that cross the trigger value on a rising edge

            if len(crossing_points) > 0:  # Don't trigger when no point crosses the trigger value

                x_trig_point = middle_element(crossing_points)
                channelA = np.roll(channelA, int(len(t)/2)-x_trig_point)
                channelB = np.roll(channelB, int(len(t)/2)-x_trig_point)

        # Show the data
        if ASettings.visible:
            aPlot.plot(t, channelA[0:len(t)], color="blue")

        if BSettings.visible:
            bPlot.plot(t, channelB[0:len(t)], color="red")

    elif settings.mode == "Power spectrum":
        aPlot.set_title("Power spectrum")
        aPlot.set_xlim(-settings.frequencyRangeShown/20, settings.frequencyRangeShown)
        aPlot.set_xlabel("Hz")

        aPlot.set_ylim(bottom=-ASettings.yLimitSpectrum/20, top=ASettings.yLimitSpectrum)
        bPlot.set_ylim(bottom=-BSettings.yLimitSpectrum/20, top=BSettings.yLimitSpectrum)

        if ASettings.visible:
            aPlot.stem(freqA, np.abs(spectrumA)**2,  'blue', markerfmt=" ", basefmt="-b")

        if BSettings.visible:
            bPlot.stem(freqB, np.abs(spectrumB)**2,  'red', markerfmt=" ", basefmt="-r")

    else:  # Voltage spectrum
        aPlot.set_title("Voltage spectrum")
        aPlot.set_xlim(-settings.frequencyRangeShown/20, settings.frequencyRangeShown)
        aPlot.set_xlabel("Hz")

        aPlot.set_ylim(bottom=-ASettings.yLimitSpectrum/20, top=ASettings.yLimitSpectrum)
        bPlot.set_ylim(bottom=-BSettings.yLimitSpectrum/20, top=BSettings.yLimitSpectrum)

        if ASettings.visible:
            aPlot.stem(freqA, np.abs(spectrumA),  'blue', markerfmt=" ", basefmt="-b")

        if BSettings.visible:
            bPlot.stem(freqB, np.abs(spectrumB),  'red', markerfmt=" ", basefmt="-r")

    # Draw cursors
    if settings.mode == "time":
        if cursorSettings.H1Visible:
            aPlot.hlines(y=cursorSettings.H1Position, xmin=t[0], xmax=t[-1], colors="grey", linestyles="--")

        if cursorSettings.H2Visible:
            aPlot.hlines(y=cursorSettings.H2Position, xmin=t[0], xmax=t[-1], colors="grey", linestyles="--")

        if cursorSettings.V1Visible:
            aPlot.vlines(x=cursorSettings.V1Position, ymin=0, ymax=max(ASettings.yLimitVoltage, BSettings.yLimitVoltage), colors="grey", linestyles="--")

        if cursorSettings.V2Visible:
            aPlot.vlines(x=cursorSettings.V2Position, ymin=0, ymax=max(ASettings.yLimitVoltage, BSettings.yLimitVoltage), colors="grey", linestyles="--")

    else:
        if cursorSettings.H1Visible:
            aPlot.hlines(y=cursorSettings.H1Position, xmin=freqA[0], xmax=freqA[-1], colors="grey", linestyles="--")

        if cursorSettings.H2Visible:
            aPlot.hlines(y=cursorSettings.H2Position, xmin=freqB[0], xmax=freqB[-1], colors="grey", linestyles="--")

        if cursorSettings.V1Visible:
            aPlot.vlines(x=cursorSettings.V1Position, ymin=0, ymax=max(ASettings.yLimitSpectrum, BSettings.yLimitSpectrum), colors="grey", linestyles="--")

        if cursorSettings.V2Visible:
            aPlot.vlines(x=cursorSettings.V2Position, ymin=0, ymax=max(ASettings.yLimitSpectrum, BSettings.yLimitSpectrum), colors="grey", linestyles="--")

    updateMeasurements()
    canvas.draw_idle()  # Updated the screen

    # Creates calls updateCanvas again after the set time
    if settings.running:
        updateJob = window.after(settings.refreshRate, updateCanvas)


def middle_element(arr):

    x1 = (len(arr)/2)  # x1 is the index
    x1 = int(x1)  # x1 float to int, truncated
    return arr[x1]


window = tk.Tk()
window.title("Oscilloscope")
# window.title("Cheap oscilloscope bit dodgy but it works")
window.columnconfigure(0, weight=1)  # Fill up the available space
window.rowconfigure(2, weight=1)

# Create frames to organize parts of the GUI
generalSettings = tk.Frame(window, highlightthickness=2)
generalSettings.grid(row=0, column=0, sticky="W", columnspan=2)
dataSettings = tk.Frame(window, highlightthickness=2)
dataSettings.grid(row=1, column=0, sticky="W", columnspan=2)
dataDisplay = tk.Frame(window, highlightthickness=2)
dataDisplay.grid(column=0, row=2, sticky="NSEW")
dataDisplay.grid_columnconfigure(0, weight=1)
triggerCursors = tk.Frame(window, highlightthickness=2)
triggerCursors.grid(column=1, row=2, sticky="N")
measurements = tk.Frame(window, highlightthickness=2)
measurements.grid(column=0, row=3)

# The matplotlib plot
fig = plt.Figure()
canvas = FigureCanvasTkAgg(fig, master=dataDisplay)
canvas.get_tk_widget().pack(fill="both", expand=True)


def forceRefresh():
    # Used to refresh the data shown on the screen
    window.after_cancel(updateJob)  # This is necessary to avoid creating more jobs
    updateCanvas()

# Functions that change general settings


def changePort(*args):
    settings.deviceName = portStringVar.get()
    settings.port = portDict[settings.deviceName]


def updateRefreshRate(*args):
    settings.refreshRate = int(1000*float(refReshRateStringVar.get()))


def onSSButton():
    if settings.running:
        settings.running = False

        SSButton.config(text="Start")
        window.after_cancel(updateJob)

    else:
        settings.running = True
        SSButton.config(text="Stop")
        updateCanvas()


def onArrangeY():
    # Find the smallest possible setting that's larger than the highest peak
    if settings.mode == "time":
        for setting in yLimitVoltageOptions:  # Channel A
            if setting > max(channelA):
                ASettings.yLimitVoltage = setting
                AYlimStringVar.set(setting)
                break

        for setting in yLimitVoltageOptions:  # Channel B
            if setting > max(channelB):
                BSettings.yLimitVoltage = setting
                BYlimStringVar.set(setting)
                break

    elif settings.mode == "Power spectrum":
        for setting in yLimitSpectrumOptions:  # Channel A
            if setting > max(np.abs(spectrumA)**2):
                ASettings.yLimitSpectrum = setting
                AYlimSpectrumStringVar.set(setting)
                break

        for setting in yLimitSpectrumOptions:  # Channel B
            if setting > max(np.abs(channelB)**2):
                BSettings.yLimitSpectrum = setting
                BYlimSpectrumStringVar.set(setting)
                break

    else:  # Voltage Spectrum mode
        for setting in yLimitSpectrumOptions:
            if setting > max(np.abs(spectrumA)):  # Channel A
                ASettings.yLimitSpectrum = setting
                AYlimSpectrumStringVar.set(setting)
                break

        for setting in yLimitSpectrumOptions:  # Channel B
            if setting > max(np.abs(channelB)):
                BSettings.yLimitSpectrum = setting
                BYlimSpectrumStringVar.set(setting)
                break

    forceRefresh()


def saveToFile():

    fileTypes = [
                ("CSV", "*.csv"),
                ("Text Document", "*.txt")
            ]

    path = tkf.asksaveasfilename(initialfile=time.strftime("%Y%m%d-%H%M%S"), defaultextension=fileTypes, filetypes=fileTypes)

    with open(path, "w", newline='') as file:
        writer = csv.writer(file, delimiter=",")
        writer.writerow(("Channel A", "Channel B"))
        for i in range(len(channelA)):
            writer.writerow((channelA[i], channelB[i]))

# Functions that change data settings


def onTimeFreqButton():
    # On button press we move to the next option in the sequence: time->Voltage spec->Power Spec
    if settings.mode == "time":
        settings.mode = "frequency"

        timeFreqButton.config(text="Power spectrum")

        # Change the settings bar to spectrum mode
        AYlimSetting.grid_forget()
        BYlimSetting.grid_forget()
        timeRangeLabel.grid_forget()
        timeRangeSetting.grid_forget()

        AYlimSpectrumSetting.grid(column=3, row=0, padx=(0, 20))
        BYlimSpectrumSetting.grid(column=6, row=0, padx=(0, 20))
        frequencyRangeSetting.grid(column=8, row=0, padx=(0, 3))
        frequencyRangeLabel.grid(column=7, row=0, padx=(0, 10))

    elif settings.mode == "frequency":
        settings.mode = "Power spectrum"
        timeFreqButton.config(text="Time")

    else:
        settings.mode = "time"
        timeFreqButton.config(text="Voltage spectrum")

        # Change settings to time mode
        AYlimSpectrumSetting.grid_forget()
        BYlimSpectrumSetting.grid_forget()

        AYlimSetting.grid(column=3, row=0, padx=(0, 20))
        BYlimSetting.grid(column=6, row=0, padx=(0, 20))
        frequencyRangeLabel.grid_forget()
        frequencyRangeSetting.grid_forget()
        timeRangeSetting.grid(column=8, row=0, padx=(0, 3))
        timeRangeLabel.grid(column=7, row=0, padx=(0, 10))

    updateDeltas()  # To change the units on the labels

    forceRefresh()


def onAVisible():
    ASettings.visible = not ASettings.visible

    if ASettings.visible:
        AVisible.config(bg="blue")
    else:
        AVisible.config(bg="SystemButtonFace")

    forceRefresh()


def onBVisible():
    BSettings.visible = not BSettings.visible

    if BSettings.visible:
        BVisible.config(bg="red")
    else:
        BVisible.config(bg="SystemButtonFace")

    forceRefresh()


def updateAYlim(*args):
    ASettings.yLimitVoltage = float(AYlimStringVar.get())
    forceRefresh()


def updateBYlim(*args):
    BSettings.yLimitVoltage = float(BYlimStringVar.get())
    forceRefresh()


def updateAYlimSpectrum(*args):
    ASettings.yLimitSpectrum = float(AYlimSpectrumStringVar.get())
    forceRefresh()


def updateBYlimSpectrum(*args):
    BSettings.yLimitSpectrum = float(BYlimSpectrumStringVar.get())
    forceRefresh()


def updateTimeRange(*args):
    settings.timeRangeShown = float(timeRangeStringVar.get())
    forceRefresh()


def updateFrequencyRange(*args):
    settings.frequencyRangeShown = float(frequencyRangeStringVar.get())
    forceRefresh()


# Functions related to cursors


def onH1(*args):

    if cursorSettings.H1Visible:
        cursorSettings.H1Visible = False
        H1Button.config(text="Show Horizontal 1")
    else:
        cursorSettings.H1Visible = True
        H1Button.config(text="Hide Horizontal 1")

    forceRefresh()


def onH2(*args):
    if cursorSettings.H2Visible:
        cursorSettings.H2Visible = False
        H2Button.config(text="Show Horizontal 2")
    else:
        cursorSettings.H2Visible = True
        H2Button.config(text="Hide Horizontal 2")
    forceRefresh()


def onV1(*args):
    if cursorSettings.V1Visible:
        cursorSettings.V1Visible = False
        V1Button.config(text="Show Vertical 1")
    else:
        cursorSettings.V1Visible = True
        V1Button.config(text="Hide Vertical 1")
    forceRefresh()


def onV2(*args):
    if cursorSettings.V2Visible:
        cursorSettings.V2Visible = False
        V2Button.config(text="Show Vertical 2")
    else:
        cursorSettings.V2Visible = True
        V2Button.config(text="Hide Vertical 2")
    forceRefresh()


def updateDeltas():
    HDelta = cursorSettings.H1Position-cursorSettings.H2Position
    VDelta = cursorSettings.V1Position-cursorSettings.V2Position
    if settings.mode == "time":
        HDeltaLabel.config(text=f"Vertical delta: {HDelta:.2f} V")
        VDeltaLabel.config(text=f"Horizontal delta: {VDelta:.2f} ms")
    else:
        HDeltaLabel.config(text=f"Vertical delta: {HDelta:.2f} V")
        VDeltaLabel.config(text=f"Horizontal delta: {VDelta:.2f} Hz")


def updateH1Pos(*args):
    cursorSettings.H1Position = float(H1StringVar.get())
    updateDeltas()
    forceRefresh()


def updateH2Pos(*args):
    cursorSettings.H2Position = float(H2StringVar.get())
    updateDeltas()
    forceRefresh()


def updateV1Pos(*args):
    cursorSettings.V1Position = float(V1StringVar.get())
    updateDeltas()
    forceRefresh()


def updateV2Pos(*args):
    cursorSettings.V2Position = float(V2StringVar.get())
    updateDeltas()
    forceRefresh()


# Functions related to triggers
def onTriggerButton(*args):
    if settings.trigger:
        settings.trigger = False
        triggerButton.config(bg="SystemButtonFace")
    else:
        settings.trigger = True

        triggerButton.config(bg="green")

    forceRefresh()


def onTriggerChannel(channel):
    if channel == "A":
        settings.triggerChannel = "A"
        triggerChannelA.config(bg="green")
        triggerChannelB.config(bg="SystemButtonFace")
    else:
        settings.triggerChannel = "B"
        triggerChannelA.config(bg="SystemButtonFace")
        triggerChannelB.config(bg="green")


def updateTriggerValue(*args):
    settings.triggerValue = float(triggerStringVar.get())
    forceRefresh()

# Functions related to measurement settings


def onM1Channel(channel):

    if channel == "A":
        measurementSettings.M1Channel = "A"
        measurement1ChannelA.config(bg="green")
        measurement1ChannelB.config(bg="SystemButtonFace")
    else:
        measurementSettings.M1Channel = "B"
        measurement1ChannelA.config(bg="SystemButtonFace")
        measurement1ChannelB.config(bg="green")


def updateMeasurement1(*args):
    measurementSettings.M1Type = measurement1StringVar.get()
    measurementSettings.M1Limited = measurement1RangeIntVar.get()
    forceRefresh()


def onM2Channel(channel):
    if channel == "A":
        measurementSettings.M2Channel = "A"
        measurement2ChannelA.config(bg="green")
        measurement2ChannelB.config(bg="SystemButtonFace")
    else:
        measurementSettings.M2Channel = "B"
        measurement2ChannelA.config(bg="SystemButtonFace")
        measurement2ChannelB.config(bg="green")


def updateMeasurement2(*args):
    measurementSettings.M2Type = measurement2StringVar.get()
    measurementSettings.M2Limited = measurement2RangeIntVar.get()
    forceRefresh()


def onM3Channel(channel):
    if channel == "A":
        measurementSettings.M3Channel = "A"
        measurement3ChannelA.config(bg="green")
        measurement3ChannelB.config(bg="SystemButtonFace")
    else:
        measurementSettings.M3Channel = "B"
        measurement3ChannelA.config(bg="SystemButtonFace")
        measurement3ChannelB.config(bg="green")


def updateMeasurement3(*args):
    measurementSettings.M3Type = measurement3StringVar.get()
    measurementSettings.M3Limited = measurement3RangeIntVar.get()
    forceRefresh()


def onM4Channel(channel):
    if channel == "A":
        measurementSettings.M4Channel = "A"
        measurement4ChannelA.config(bg="green")
        measurement4ChannelB.config(bg="SystemButtonFace")
    else:
        measurementSettings.M4Channel = "B"
        measurement4ChannelA.config(bg="SystemButtonFace")
        measurement4ChannelB.config(bg="green")


def updateMeasurement4(*args):
    measurementSettings.M4Type = measurement4StringVar.get()
    measurementSettings.M4Limited = measurement4RangeIntVar.get()
    forceRefresh()


# General settings GUI elements
portStringVar = tk.StringVar(window)
portStringVar.set(settings.deviceName)
portSetting = tk.OptionMenu(generalSettings, portStringVar, *portDict.keys())
portSetting.config(width=20, padx=0)
portStringVar.trace("w", changePort)
portSetting.grid(column=0, row=0, padx=(0, 3))

tk.Label(generalSettings, text="Refresh time: (s)", padx=3).grid(column=1, row=0, padx=(3, 0))

refReshRateStringVar = tk.StringVar(window)
refReshRateStringVar.set(screenRefreshRateOptions[3])
refReshRateSetting = tk.OptionMenu(generalSettings, refReshRateStringVar, *screenRefreshRateOptions)
refReshRateSetting.config(width=5)
refReshRateStringVar.trace("w", updateRefreshRate)
refReshRateSetting.grid(column=2, row=0, padx=(0, 3))

SSButton = tk.Button(generalSettings, text="Stop", command=onSSButton, width=20)
SSButton.grid(column=4, row=0, padx=3)

arrangeYButton = tk.Button(generalSettings, text="Arrange Y axes", command=onArrangeY)
arrangeYButton.grid(column=5, row=0, padx=3)

saveToFileButton = tk.Button(generalSettings, text="Save to File", command=saveToFile)
saveToFileButton.grid(column=6, row=0, padx=3)

# Data settings GUI elements

timeFreqButton = tk.Button(dataSettings, text="Voltage Spectrum", command=onTimeFreqButton, width=15)
timeFreqButton.grid(row=0, column=0, padx=(0, 10))

AVisible = tk.Button(dataSettings, text="A", command=onAVisible, bg="blue")
AVisible.grid(row=0, column=1, padx=(20, 3))
BVisible = tk.Button(dataSettings, text="B", command=onBVisible, bg="red")
BVisible.grid(row=0, column=4, padx=(20, 3))

tk.Label(dataSettings, text="Y-axis limit: (V)").grid(column=2, row=0, padx=(3, 0))
tk.Label(dataSettings, text="Y-axis limit: (V)").grid(column=5, row=0, padx=(3, 0))

# Time domain limits
AYlimStringVar = tk.StringVar(window)
AYlimStringVar.set(yLimitVoltageOptions[-2])
AYlimSetting = tk.OptionMenu(dataSettings, AYlimStringVar, *yLimitVoltageOptions)
AYlimSetting.config(width=5)
AYlimStringVar.trace("w", updateAYlim)
AYlimSetting.grid(column=3, row=0, padx=(0, 20))

BYlimStringVar = tk.StringVar(window)
BYlimStringVar.set(yLimitVoltageOptions[-2])
BYlimSetting = tk.OptionMenu(dataSettings, BYlimStringVar, *yLimitVoltageOptions)
BYlimSetting.config(width=5)
BYlimStringVar.trace("w", updateBYlim)
BYlimSetting.grid(column=6, row=0, padx=(0, 20))

timeRangeLabel = tk.Label(dataSettings, text="Time period shown: (ms) ", width=25, justify="right")
timeRangeLabel.grid(column=7, row=0, padx=(0, 10))
timeRangeStringVar = tk.StringVar(window)
timeRangeStringVar.set(timeRangeOptions[-1])
timeRangeSetting = tk.OptionMenu(dataSettings, timeRangeStringVar, *timeRangeOptions)
timeRangeSetting.config(width=5, padx=3)
timeRangeStringVar.trace("w", updateTimeRange)
timeRangeSetting.grid(column=8, row=0, padx=(0, 3))


# Frequency domain limits (hidden on startup, visible when in spectrum mode)

AYlimSpectrumStringVar = tk.StringVar(window)
AYlimSpectrumStringVar.set(yLimitSpectrumOptions[-1])
AYlimSpectrumSetting = tk.OptionMenu(dataSettings, AYlimSpectrumStringVar, *yLimitSpectrumOptions)
AYlimSpectrumSetting.config(width=5)
AYlimSpectrumStringVar.trace("w", updateAYlimSpectrum)
AYlimSpectrumSetting.grid(column=3, row=0, padx=(0, 20))
AYlimSpectrumSetting.grid_forget()

BYlimSpectrumStringVar = tk.StringVar(window)
BYlimSpectrumStringVar.set(yLimitSpectrumOptions[-1])
BYlimSpectrumSetting = tk.OptionMenu(dataSettings, BYlimSpectrumStringVar, *yLimitSpectrumOptions)
BYlimSpectrumSetting.config(width=5)
BYlimSpectrumStringVar.trace("w", updateBYlimSpectrum)
BYlimSpectrumSetting.grid(column=6, row=0, padx=(0, 20))
BYlimSpectrumSetting.grid_forget()

frequencyRangeLabel = tk.Label(dataSettings, text="Frequency range shown: (Hz) ", width=25, justify="right")
frequencyRangeLabel.grid(column=7, row=0, padx=(0, 10))
frequencyRangeStringVar = tk.StringVar(window)
frequencyRangeStringVar.set(frequencyRangeOptions[-1])
frequencyRangeSetting = tk.OptionMenu(dataSettings, frequencyRangeStringVar, *frequencyRangeOptions)
frequencyRangeSetting.config(width=5, padx=3)
frequencyRangeStringVar.trace("w", updateFrequencyRange)
frequencyRangeSetting.grid(column=8, row=0, padx=(0, 3))
frequencyRangeLabel.grid_forget()
frequencyRangeSetting.grid_forget()

# Cursor GUI elements

# Horizontal 1
H1Button = tk.Button(triggerCursors, text="Show Horizontal 1", command=onH1, width=20)
H1Button.grid(column=0, row=0, pady=3, columnspan=2)
H1StringVar = tk.StringVar(window)
H1StringVar.set(cursorSettings.H1Position)
H1Position = tk.Entry(triggerCursors,  textvariable=H1StringVar, justify="center")
H1Position.grid(column=0, row=1, pady=(3, 10), columnspan=2)
H1StringVar.trace("w", updateH1Pos)

# Horizontal 2
H2Button = tk.Button(triggerCursors, text="Show Horizontal 2", command=onH2, width=20)
H2Button.grid(column=0, row=2, pady=(10, 3), columnspan=2)
H2StringVar = tk.StringVar(window)
H2StringVar.set(cursorSettings.H2Position)
H2Position = tk.Entry(triggerCursors,  textvariable=H2StringVar, justify="center")
H2Position.grid(column=0, row=3, pady=(3, 3), columnspan=2)
H2StringVar.trace("w", updateH2Pos)

HDeltaLabel = tk.Label(triggerCursors, text="Vertical delta: 0 V")
HDeltaLabel.grid(column=0, row=4, pady=(10, 20), columnspan=2)


# Vertical 1
V1Button = tk.Button(triggerCursors, text="Show Vertical 1", command=onV1, width=20)
V1Button.grid(column=0, row=5, pady=(20, 3), columnspan=2)
V1StringVar = tk.StringVar(window)
V1StringVar.set(cursorSettings.V1Position)
V1Position = tk.Entry(triggerCursors,  textvariable=V1StringVar, justify="center")
V1Position.grid(column=0, row=6, pady=(3, 10), columnspan=2)
V1StringVar.trace("w", updateV1Pos)

# Vertical 2
V2Button = tk.Button(triggerCursors, text="Show Vertical 2", command=onV2, width=20)
V2Button.grid(column=0, row=7, pady=(10, 3), columnspan=2)
V2StringVar = tk.StringVar(window)
V2StringVar.set(cursorSettings.V2Position)
V2Position = tk.Entry(triggerCursors,  textvariable=V2StringVar, justify="center")
V2Position.grid(column=0, row=8, pady=(3, 3), columnspan=2)
V2StringVar.trace("w", updateV2Pos)

VDeltaLabel = tk.Label(triggerCursors, text="Horizontal delta: 0 ms")
VDeltaLabel.grid(column=0, row=9, pady=(10, 20), columnspan=2)

# Trigger GUI elements
triggerButton = tk.Button(triggerCursors, text="Trigger", command=onTriggerButton, width=20)
triggerButton.grid(column=0, row=10, pady=(20, 3), columnspan=2)

triggerChannelA = tk.Button(triggerCursors, text="A", command=lambda: onTriggerChannel("A"), width=8, bg="green")
triggerChannelA.grid(column=0, row=11, pady=(3, 3))
triggerChannelB = tk.Button(triggerCursors, text="B", command=lambda: onTriggerChannel("B"), width=8)
triggerChannelB.grid(column=1, row=11, pady=(3, 3))


triggerStringVar = tk.StringVar(window)
triggerStringVar.set(settings.triggerValue)
triggerEntry = tk.Entry(triggerCursors,  textvariable=triggerStringVar, justify="center")
triggerEntry.grid(column=0, row=12, pady=(3, 10), columnspan=2)
triggerStringVar.trace("w", updateTriggerValue)


# Measurement GUI elements

# Measurement1
measurement1ChannelA = tk.Button(measurements, text="A", command=lambda: onM1Channel("A"), width=8, bg="green")
measurement1ChannelA.grid(column=0, row=0)
measurement1ChannelB = tk.Button(measurements, text="B", command=lambda: onM1Channel("B"), width=8)
measurement1ChannelB.grid(column=1, row=0)
measurement1StringVar = tk.StringVar(window)
measurement1StringVar.set("Off")
measurement1Setting = tk.OptionMenu(measurements, measurement1StringVar, *measurementOptions)
measurement1Setting.config(width=20)
measurement1StringVar.trace("w", updateMeasurement1)
measurement1Setting.grid(column=0, row=1, columnspan=2)
measurement1RangeIntVar = tk.IntVar(window)
tk.Checkbutton(measurements, text="Between cursors", variable=measurement1RangeIntVar).grid(column=0, row=2, columnspan=2)
measurement1RangeIntVar.trace("w", updateMeasurement1)
measurement1Label = tk.Label(measurements, text="")
measurement1Label.grid(column=0, row=3, columnspan=2)

# Measurement2
measurement2ChannelA = tk.Button(measurements, text="A", command=lambda: onM2Channel("A"), width=8, bg="green")
measurement2ChannelA.grid(column=2, row=0)
measurement2ChannelB = tk.Button(measurements, text="B", command=lambda: onM2Channel("B"), width=8)
measurement2ChannelB.grid(column=3, row=0)
measurement2StringVar = tk.StringVar(window)
measurement2StringVar.set("Off")
measurement2Setting = tk.OptionMenu(measurements, measurement2StringVar, *measurementOptions)
measurement2Setting.config(width=20)
measurement2StringVar.trace("w", updateMeasurement2)
measurement2Setting.grid(column=2, row=1, columnspan=2)
measurement2RangeIntVar = tk.IntVar(window)
tk.Checkbutton(measurements, text="Between cursors", variable=measurement2RangeIntVar).grid(column=2, row=2, columnspan=2)
measurement2RangeIntVar.trace("w", updateMeasurement2)
measurement2Label = tk.Label(measurements, text="")
measurement2Label.grid(column=2, row=3, columnspan=2)

# Measurement3
measurement3ChannelA = tk.Button(measurements, text="A", command=lambda: onM3Channel("A"), width=8, bg="green")
measurement3ChannelA.grid(column=4, row=0)
measurement3ChannelB = tk.Button(measurements, text="B", command=lambda: onM3Channel("B"), width=8)
measurement3ChannelB.grid(column=5, row=0)
measurement3StringVar = tk.StringVar(window)
measurement3StringVar.set("Off")
measurement3Setting = tk.OptionMenu(measurements, measurement3StringVar, *measurementOptions)
measurement3Setting.config(width=20)
measurement3StringVar.trace("w", updateMeasurement3)
measurement3Setting.grid(column=4, row=1, columnspan=2)
measurement3RangeIntVar = tk.IntVar(window)
tk.Checkbutton(measurements, text="Between cursors", variable=measurement3RangeIntVar).grid(column=4, row=2, columnspan=2)
measurement3RangeIntVar.trace("w", updateMeasurement3)
measurement3Label = tk.Label(measurements, text="")
measurement3Label.grid(column=4, row=3, columnspan=2)

# Measurement4
measurement4ChannelA = tk.Button(measurements, text="A", command=lambda: onM4Channel("A"), width=8, bg="green")
measurement4ChannelA.grid(column=6, row=0)
measurement4ChannelB = tk.Button(measurements, text="B", command=lambda: onM4Channel("B"), width=8)
measurement4ChannelB.grid(column=7, row=0)
measurement4StringVar = tk.StringVar(window)
measurement4StringVar.set("Off")
measurement4Setting = tk.OptionMenu(measurements, measurement4StringVar, *measurementOptions)
measurement4Setting.config(width=20)
measurement4StringVar.trace("w", updateMeasurement4)
measurement4Setting.grid(column=6, row=1, columnspan=2)
measurement4RangeIntVar = tk.IntVar(window)
tk.Checkbutton(measurements, text="Between cursors", variable=measurement4RangeIntVar).grid(column=6, row=2, columnspan=2)
measurement4RangeIntVar.trace("w", updateMeasurement4)
measurement4Label = tk.Label(measurements, text="")
measurement4Label.grid(column=6, row=3, columnspan=2)


# Use two different threads, one for serial communications, one for data processing and GUI
window.after(100, _thread.start_new_thread, readSerialInput, ())
window.after(100, _thread.start_new_thread, updateCanvas, ())

window.mainloop()
