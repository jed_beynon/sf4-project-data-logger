void setup() {
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  Serial.begin(1000000);
}


unsigned long currentMicros = 0;
unsigned long previousMicros = 0;

int encodeUpper(int number)
{
  return (number>>4) & 0x3C;
}

int encodeLower(int number)
{
  return (number<<2) & 0xFC;
}


float voltage = 0;
int channelA = 0;
int channelB = 0;
int dataID  = 0;
int samplingTime = 0;
void loop() {  
  currentMicros = micros();
  
  if(currentMicros-previousMicros > 250)
  {
    channelA = analogRead(A0);
    channelB = analogRead(A1);
    
    Serial.write(encodeUpper(dataID));
    Serial.write(encodeLower(dataID)); 
    
    Serial.write( encodeUpper(channelA));
    Serial.write( encodeLower(channelA));
    
    Serial.write( encodeUpper(channelB));
    Serial.write( encodeLower(channelB));
    
    if(dataID == 1023)
    {
        samplingTime = currentMicros-previousMicros;
        Serial.write( encodeUpper(samplingTime));
        Serial.write( encodeLower(samplingTime));
    }

    Serial.write("\n");

    previousMicros = currentMicros;
    dataID += 1;
    
    if(dataID == 1024)
      dataID = 0; 
  }
}
